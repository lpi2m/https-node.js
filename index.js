var express = require('express');
var fs = require('fs');
var https = require('https');

var privateKey  = fs.readFileSync('certs/[nom de la clé].key', 'utf8');
var certificate = fs.readFileSync('certs/[nom du certificat].crt', 'utf8');

var credentials = {key: privateKey, cert: certificate};
var app = express();

app.get('/', function(req, res){
    res.send("Yolo !");
    res.status = 200;
});

var httpsServer = https.createServer(credentials, app);
httpsServer.listen(443);