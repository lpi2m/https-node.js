Objectif
========

Ce repository contient le strict minimum pour mettre en place un serveur HTTPS utilisant le framework Express (Node.js), à savoir :

* le fichier index.js
* le descripteur package.json
* le tutoriel pour générer la clé et le certificat
* le tutoriel pour mettre en place l'application

Génération de la clé et du certificat
=====================================

La clé
------

    openssl genrsa -out [nom de la clé].key [taille de la clé (1024)]
    
Demande de génération de certificat
-----------------------------------

    openssl req -new -key [nom de la clé].key -out [nom de la demande].csr
    
**Attention :** Des informations seront demandés pour pouvoir ensuite générer le certificat. Le Common Name doit être identique au domaine (ou sous-domaine) qui hébergera le serveur. Il ne faut pas indiquer de port !

Génération du certificat
------------------------

    openssl x509 -req -days [durée de validité en jours] -in [nom de la demande].csr -signkey [nom de la clé].key -out [nom du certificat].crt
    
Mise en place de l'application
==============================

Il faut copier la clé (.key) et le certificat (.crt) dans le dossier `certs` de l'application. Il faut ensuite éditer le fichier `index.js`, à la ligne 5 et 6 :

    var privateKey  = fs.readFileSync('certs/[nom de la clé].key', 'utf8');
    var certificate = fs.readFileSync('certs/[nom du certificat].crt', 'utf8');
    
Il faut ensuite installer les dépendances (Express) :

    npm install
    
Voilà, le serveur est prêt :

    node index.js
    
Il écoute désormais sur le port 443 de toutes les interfaces de la machine.